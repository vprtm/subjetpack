package android.app.submission1jetpack.ui

import android.app.submission1jetpack.databinding.ActivitySplashScreenBinding
import android.app.submission1jetpack.utils.setFull
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class SplashScreenActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window?.setFull()

        GlobalScope.launch {
            delay(3000)
            MainActivity.start(this@SplashScreenActivity)
            finish()
        }
    }
}