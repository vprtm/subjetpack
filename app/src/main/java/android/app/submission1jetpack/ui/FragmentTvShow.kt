package android.app.submission1jetpack.ui

import android.app.submission1jetpack.adapter.FavoriteTvShowAdapter
import android.app.submission1jetpack.adapter.TvShowAdapter
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.databinding.FragmentTvShowBinding
import android.app.submission1jetpack.utils.BundleKeys
import android.app.submission1jetpack.utils.Resource
import android.app.submission1jetpack.utils.gone
import android.app.submission1jetpack.utils.visible
import android.app.submission1jetpack.viewmodel.TvShowViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentTvShow : Fragment() {

    companion object {
        @JvmStatic
        fun create(isFavorite: Boolean = false) = FragmentTvShow().apply {
            arguments = Bundle().apply {
                putBoolean(BundleKeys.IS_FAVORITE, isFavorite)
            }
        }
    }

    private val tvShows: MutableList<TvShow> = mutableListOf()

    private lateinit var tvShowAdapter: TvShowAdapter

    private lateinit var tvShowFavoriteAdapter: FavoriteTvShowAdapter

    private lateinit var binding: FragmentTvShowBinding

    private val tvShowViewModel: TvShowViewModel by viewModel()

    private var favoritePage = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        favoritePage = arguments?.getBoolean(BundleKeys.IS_FAVORITE, false) ?: false
        binding = FragmentTvShowBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (favoritePage) setupFavoritePage() else setupAllPage()
    }

    private fun setupFavoritePage(){
        tvShowFavoriteAdapter = FavoriteTvShowAdapter(requireContext())

        binding.rvTvShow.apply {
            adapter = tvShowFavoriteAdapter
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        tvShowViewModel.getAllFavoriteTvShows().observe(viewLifecycleOwner, {
            tvShowFavoriteAdapter.submitList(it)
        })
    }

    private fun setupAllPage() {
        tvShowAdapter = TvShowAdapter(requireContext(), tvShows)

        binding.rvTvShow.apply {
            adapter = tvShowAdapter
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        tvShowViewModel.getTvShows("en-US", 1)

        tvShowViewModel.tvShows.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {
                    binding.rvTvShow.gone()
                    binding.loadingAnim.visible()
                }
                is Resource.Success -> {
                    binding.rvTvShow.visible()
                    binding.loadingAnim.gone()
                    tvShows.addAll(it.data)
                }
                is Resource.Error -> {
                    binding.rvTvShow.visible()
                    binding.loadingAnim.gone()
                    Toast.makeText(requireContext(), it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })
    }
}