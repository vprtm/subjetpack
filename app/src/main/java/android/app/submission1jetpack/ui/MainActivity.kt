package android.app.submission1jetpack.ui

import android.app.submission1jetpack.R
import android.app.submission1jetpack.adapter.ViewPagerAdapter
import android.app.submission1jetpack.databinding.ActivityMainBinding
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context) {
            Intent(context, MainActivity::class.java).apply {
                context.startActivity(this)
            }
        }
    }

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarMain)

        setupViewPagerMain()

        binding.fabFavorite.setOnClickListener {
            FavoriteActivity.start(this)
        }
    }

    private fun setupViewPagerMain() {
        val pages = listOf(
            FragmentMovie.create(),
            FragmentTvShow.create()
        )

        val titles = listOf(
            getString(R.string.title_movie),
            getString(R.string.title_tv_show)
        )

        binding.vpMain.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.vpMain.adapter = ViewPagerAdapter(pages, supportFragmentManager, lifecycle)
        binding.vpMain.offscreenPageLimit = 2
        binding.vpMain.isUserInputEnabled = true

        TabLayoutMediator(binding.tabLayoutMain, binding.vpMain) { tab, position ->
            tab.text = titles[position]
        }.attach()
    }
}