package android.app.submission1jetpack.ui

import android.app.submission1jetpack.adapter.FavoriteMovieAdapter
import android.app.submission1jetpack.adapter.MovieAdapter
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.databinding.FragmentMovieBinding
import android.app.submission1jetpack.utils.BundleKeys
import android.app.submission1jetpack.utils.Resource
import android.app.submission1jetpack.utils.gone
import android.app.submission1jetpack.utils.visible
import android.app.submission1jetpack.viewmodel.MovieViewModel
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import org.koin.androidx.viewmodel.ext.android.viewModel

class FragmentMovie : Fragment() {

    companion object {
        @JvmStatic
        fun create(isFavorite : Boolean = false) = FragmentMovie().apply {
            arguments = Bundle().apply {
                putBoolean(BundleKeys.IS_FAVORITE, isFavorite)
            }
        }
    }

    private val movies: MutableList<Movie> = mutableListOf()

    private lateinit var movieAdapter: MovieAdapter

    private lateinit var movieFavoriteAdapter: FavoriteMovieAdapter

    private lateinit var binding: FragmentMovieBinding

    private val movieViewModel: MovieViewModel by viewModel()

    private var favoritePage = false

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        favoritePage = arguments?.getBoolean(BundleKeys.IS_FAVORITE, false) ?: false
        binding = FragmentMovieBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        if (favoritePage) setupFavoritePage() else setupAllPage()
    }

    private fun setupFavoritePage(){
        movieFavoriteAdapter = FavoriteMovieAdapter(requireContext())

        binding.rvMovie.apply {
            adapter = movieFavoriteAdapter
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        movieViewModel.getAllFavoriteMovies().observe(viewLifecycleOwner, {
            movieFavoriteAdapter.submitList(it)
        })
    }

    private fun setupAllPage(){
        movieAdapter = MovieAdapter(requireContext(), movies)

        binding.rvMovie.apply {
            adapter = movieAdapter
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        }

        movieViewModel.movies.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> {
                    binding.rvMovie.gone()
                    binding.loadingAnim.visible()
                }
                is Resource.Success -> {
                    binding.rvMovie.visible()
                    binding.loadingAnim.gone()
                    movies.addAll(it.data)
                }
                is Resource.Error -> {
                    binding.rvMovie.visible()
                    binding.loadingAnim.gone()
                    Toast.makeText(requireContext(), it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        movieViewModel.getMovies("en-US", 1)
    }
}