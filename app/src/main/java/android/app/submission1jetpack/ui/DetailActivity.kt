package android.app.submission1jetpack.ui

import android.app.submission1jetpack.R
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.databinding.ActivityDetailBinding
import android.app.submission1jetpack.utils.*
import android.app.submission1jetpack.viewmodel.MovieDetailViewModel
import android.app.submission1jetpack.viewmodel.TvShowDetailViewModel
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import com.google.android.material.appbar.AppBarLayout
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailActivity : AppCompatActivity() {

    companion object {

        fun startMovieDetail(context: Context, movieId: Int) {
            Intent(context, DetailActivity::class.java).apply {
                this.putExtra(BundleKeys.MOVIE_ID, movieId)
                context.startActivity(this)
            }
        }

        fun startTvShowDetail(context: Context, tvShowId: Int) {
            Intent(context, DetailActivity::class.java).apply {
                this.putExtra(BundleKeys.TV_SHOW_ID, tvShowId)
                context.startActivity(this)
            }
        }
    }

    private lateinit var binding: ActivityDetailBinding

    private val tvShowDetailViewModel: TvShowDetailViewModel by viewModel()

    private val movieDetailViewModel: MovieDetailViewModel by viewModel()

    private var movieId = 0

    private var tvShowId = 0

    private var isFavorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        movieId = intent.getIntExtra(BundleKeys.MOVIE_ID, 0)
        tvShowId = intent.getIntExtra(BundleKeys.TV_SHOW_ID, 0)

        initObserver()

        if (movieId != 0) {
            movieDetailViewModel.setMovieId(movieId)
            movieDetailViewModel.getMovieDetail("en-US")
        } else {
            tvShowDetailViewModel.setTvShowId(tvShowId)
            tvShowDetailViewModel.getTvShowDetail("en-US")
        }

    }

    private fun setupUI(
        binding: ActivityDetailBinding,
        movie: Movie? = null,
        tvShow: TvShow? = null
    ) {
        with(binding) {
            window?.setFull()
            setSupportActionBar(toolbar)
            supportActionBar?.title = tvShow?.name ?: movie?.title
            tvName.text = tvShow?.name ?: movie?.title
            tvRate.text = (tvShow?.vote_average ?: movie?.vote_average).toString()
            tvDate.text = (tvShow?.first_air_date ?: movie?.release_date)?.toViewDate(true)
            imgBackdrop.setImageUrl(
                this@DetailActivity,
                imageBaseUrl() + (tvShow?.backdrop_path ?: movie?.backdrop_path).toString(),
                pbBackdrop
            )
            imgPoster.setImageUrl(
                this@DetailActivity,
                imageBaseUrl() + (tvShow?.poster_path ?: movie?.poster_path).toString(),
                pbPoster
            )
            tvPopularity.text = (tvShow?.popularity ?: movie?.popularity).toString()
            tvRateCount.text = (tvShow?.vote_count ?: movie?.vote_count).toString()
            tvOverview.text = tvShow?.overview ?: movie?.overview

            appBarDetail.addOnOffsetChangedListener(object :
                AppBarLayout.OnOffsetChangedListener {
                var isShow = true
                var scrollRange = -1

                override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                    if (scrollRange == -1) {
                        scrollRange = appBarLayout.totalScrollRange
                    }
                    if (scrollRange + verticalOffset == 0) {
                        toolbar.title = tvShow?.name ?: movie?.title
                        isShow = true
                    } else if (isShow) {
                        toolbar.title = emptyString()
                        isShow = false
                    }
                }
            })
        }

        binding.btnLove.setOnClickListener {
            binding.btnLove.isChecked = isFavorite
            movie?.let {
                if (isFavorite) movieDetailViewModel.deleteFromFavorite(it)
                else movieDetailViewModel.addToFavorite(movie)
            }
            tvShow?.let {
                if (isFavorite) tvShowDetailViewModel.deleteFromFavorite(it)
                else tvShowDetailViewModel.addToFavorite(tvShow)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun initObserver() {
        movieDetailViewModel.movieDetail.observe(this, {
            when (it) {
                is Resource.Loading -> {
                    binding.loadingAnim.visible()
                }
                is Resource.Success -> {
                    binding.loadingAnim.gone()
                    setupUI(binding = binding, movie = it.data)
                    movieDetailViewModel.getFavorite(movieId)
                }
                is Resource.Error -> {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        tvShowDetailViewModel.tvShowDetail.observe(this, {
            when (it) {
                is Resource.Loading -> {
                    binding.loadingAnim.visible()
                }
                is Resource.Success -> {
                    binding.loadingAnim.gone()
                    setupUI(binding = binding, tvShow = it.data)
                    tvShowDetailViewModel.getFavorite(tvShowId)
                }
                is Resource.Error -> {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        movieDetailViewModel.addFavorite.observe(this, {
            when (it) {
                is Resource.Success -> {
                    isFavorite = true
                    binding.btnLove.isChecked = isFavorite
                    Toast.makeText(
                        this,
                        getString(R.string.message_add_to_favorite),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is Resource.Error -> {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        tvShowDetailViewModel.addFavorite.observe(this, {
            when (it) {
                is Resource.Success -> {
                    isFavorite = true
                    binding.btnLove.isChecked = isFavorite
                    Toast.makeText(
                        this,
                        getString(R.string.message_add_to_favorite),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is Resource.Error -> {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        movieDetailViewModel.deleteFavorite.observe(this, Observer {
            when (it) {
                is Resource.Success -> {
                    isFavorite = false
                    binding.btnLove.isChecked = isFavorite
                    Toast.makeText(
                        this,
                        getString(R.string.delete_from_favorite),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is Resource.Error -> {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        tvShowDetailViewModel.deleteFavorite.observe(this, Observer {
            when (it) {
                is Resource.Success -> {
                    isFavorite = false
                    binding.btnLove.isChecked = isFavorite
                    Toast.makeText(
                        this,
                        getString(R.string.delete_from_favorite),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is Resource.Error -> {
                    Toast.makeText(this, it.error.message, Toast.LENGTH_SHORT).show()
                }
            }
        })

        movieDetailViewModel.isFavorite.observe(this, {
            when (it) {
                is Resource.Success -> {
                    isFavorite = it.data
                    binding.btnLove.isChecked = isFavorite
                }
            }
        })

        tvShowDetailViewModel.isFavorite.observe(this, {
            when (it) {
                is Resource.Success -> {
                    isFavorite = it.data
                    binding.btnLove.isChecked = isFavorite
                }
            }
        })
    }
}