package android.app.submission1jetpack.ui

import android.app.submission1jetpack.R
import android.app.submission1jetpack.adapter.ViewPagerAdapter
import android.app.submission1jetpack.databinding.ActivityFavoriteBinding
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator

class FavoriteActivity : AppCompatActivity() {

    companion object {
        fun start(context: Context) {
            Intent(context, FavoriteActivity::class.java).apply {
                context.startActivity(this)
            }
        }
    }

    private lateinit var binding: ActivityFavoriteBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_favorite)

        binding = ActivityFavoriteBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setSupportActionBar(binding.toolbarFavorite)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        setupViewPagerFavorite()
    }

    private fun setupViewPagerFavorite() {
        val pages = listOf(
            FragmentMovie.create(true),
            FragmentTvShow.create(true)
        )

        val titles = listOf(
            getString(R.string.title_movie),
            getString(R.string.title_tv_show)
        )

        binding.vpFavorite.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.vpFavorite.adapter = ViewPagerAdapter(pages, supportFragmentManager, lifecycle)
        binding.vpFavorite.offscreenPageLimit = 2
        binding.vpFavorite.isUserInputEnabled = true

        TabLayoutMediator(binding.tabLayoutFavorite, binding.vpFavorite) { tab, position ->
            tab.text = titles[position]
        }.attach()
    }
}