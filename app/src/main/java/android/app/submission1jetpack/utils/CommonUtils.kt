package android.app.submission1jetpack.utils

import android.view.Window
import android.view.WindowManager
import java.text.SimpleDateFormat
import java.util.*

fun imageBaseUrl() = "https://image.tmdb.org/t/p/w500"

fun emptyString() = ""

fun String.toViewDate(isFullMonthName : Boolean = false): String {
    return if (isNotEmpty() && isFullMonthName) {
        val format = SimpleDateFormat(
            if (this.length <= 11) "yyyy-MM-dd" else "yyyy-MM-dd HH:mm:ss",
            Locale.US
        )
        val tempDate = format.parse(this)

        String.format(
            "%d %s %s",
            tempDate?.date,
            SimpleDateFormat("MMMM", Locale.US).format(tempDate?.time),
            SimpleDateFormat("yyyy", Locale.US).format(tempDate?.time)
        )
    }
    else if (isNotEmpty()){
        val format = SimpleDateFormat(
            if (this.length <= 11) "yyyy-MM-dd" else "yyyy-MM-dd HH:mm:ss",
            Locale.US
        )
        val tempDate = format.parse(this)

        String.format(
            "%d %s %s",
            tempDate?.date,
            SimpleDateFormat("MMM", Locale.US).format(tempDate?.time),
            SimpleDateFormat("yyyy", Locale.US).format(tempDate?.time)
        )
    }
    else emptyString()
}

fun Window.setFull(){
    this.setFlags(
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
        WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
    )
}