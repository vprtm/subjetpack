package android.app.submission1jetpack.utils

import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow

object DataDummy {
    fun generateMovie() = Movie(
        adult = false,
        backdrop_path = "/9yBVqNruk6Ykrwc32qrK2TIE5xw.jpg",
        genre_ids = listOf(14, 28, 12, 878, 53),
        id = 460465,
        original_language = "en",
        original_title = "Mortal Kombat",
        overview = "Washed-up MMA fighter Cole Young, unaware of his heritage, and hunted by Emperor Shang Tsung's best warrior, Sub-Zero, seeks out and trains with Earth's greatest champions as he prepares to stand against the enemies of Outworld in a high stakes battle for the universe.",
        popularity = 7966.615,
        poster_path = "/6Wdl9N6dL0Hi0T1qJLWSz6gMLbd.jpg",
        release_date = "2021 - 04-07",
        title = "Mortal Kombat",
        video = false,
        vote_average = 7.9,
        vote_count = 1747
    )

    fun generateMovies() = listOf(
        Movie(
            adult = false,
            backdrop_path = "/9yBVqNruk6Ykrwc32qrK2TIE5xw.jpg",
            genre_ids = listOf(14, 28, 12, 878, 53),
            id = 460465,
            original_language = "en",
            original_title = "Mortal Kombat",
            overview = "Washed-up MMA fighter Cole Young, unaware of his heritage, and hunted by Emperor Shang Tsung's best warrior, Sub-Zero, seeks out and trains with Earth's greatest champions as he prepares to stand against the enemies of Outworld in a high stakes battle for the universe.",
            popularity = 7966.615,
            poster_path = "/6Wdl9N6dL0Hi0T1qJLWSz6gMLbd.jpg",
            release_date = "2021 - 04-07",
            title = "Mortal Kombat",
            video = false,
            vote_average = 7.9,
            vote_count = 1747
        ),
        Movie(
            adult = false,
            backdrop_path = "/inJjDhCjfhh3RtrJWBmmDqeuSYC.jpg",
            genre_ids = listOf(
                878,
                28,
                18
            ),
            id = 399566,
            original_language = "en",
            original_title = "Godzilla vs. Kong",
            overview = "In a time when monsters walk the Earth, humanity’s fight for its future sets Godzilla and Kong on a collision course that will see the two most powerful forces of nature on the planet collide in a spectacular battle for the ages.",
            popularity = 3242.331,
            poster_path = "/pgqgaUx1cJb5oZQQ5v0tNARCeBp.jpg",
            release_date = "2021-03-24",
            title = "Godzilla vs. Kong",
            video = false,
            vote_average = 8.2,
            vote_count = 5071
        )
    )


    fun generateTvShows() = listOf(
        TvShow(
            backdrop_path = "/z59kJfcElR9eHO9rJbWp4qWMuee.jpg",
            first_air_date = "2014-10-07",
            genre_ids = listOf(
                18,
                10765
            ),
            id = 60735,
            name = "The Flash",
            origin_country = listOf(
                "US"
            ),
            original_language = "en",
            original_name = "The Flash",
            overview = "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
            popularity = 1305.912,
            poster_path = "/lJA2RCMfsWoskqlQhXPSLFQGXEJ.jpg",
            vote_average = 7.7,
            vote_count = 7463
        ),
        TvShow(
            backdrop_path = "/wkyzeBBKLhSg1Oqhky5yoiFF2hG.jpg",
            first_air_date = "2018-04-22",
            genre_ids = listOf(
                18
            ),
            id = 79008,
            name = "Luis Miguel= The Series",
            origin_country = listOf(
                "MX"
            ),
            original_language = "es",
            original_name = "Luis Miguel= La Serie",
            overview = "The series dramatizes the life story of Mexican superstar singer Luis Miguel, who has captivated audiences in Latin America and beyond for decades.",
            popularity = 1026.46,
            poster_path = "/34FaY8qpjBAVysSfrJ1l7nrAQaD.jpg",
            vote_average = 8.1,
            vote_count = 332
        )
    )

    fun generateTvShow() = TvShow(
        backdrop_path = "/z59kJfcElR9eHO9rJbWp4qWMuee.jpg",
        first_air_date = "2014-10-07",
        genre_ids = listOf(
            18,
            10765
        ),
        id = 60735,
        name = "The Flash",
        origin_country = listOf(
            "US"
        ),
        original_language = "en",
        original_name = "The Flash",
        overview = "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.",
        popularity = 1305.912,
        poster_path = "/lJA2RCMfsWoskqlQhXPSLFQGXEJ.jpg",
        vote_average = 7.7,
        vote_count = 7463
    )
}