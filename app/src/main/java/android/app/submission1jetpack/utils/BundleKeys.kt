package android.app.submission1jetpack.utils

object BundleKeys {
    const val MOVIE_ID = "movie_id"
    const val TV_SHOW_ID = "tv_show_id"
    const val IS_FAVORITE = "is_favorite"
}