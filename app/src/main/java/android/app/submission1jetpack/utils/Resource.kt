package android.app.submission1jetpack.utils

sealed class Resource<out T> {
    data class Loading<out T>(val loading : Boolean) : Resource<T>()
    data class Success<out T>(val data: T) : Resource<T>()
    class Error(val error: android.app.submission1jetpack.data.base.Error) : Resource<Nothing>()
    class Empty<out T>(val data : T) : Resource<T>()
    data class Init<out T>(val new : Boolean) : Resource<T>()

    companion object {
        fun <T> loading(): Resource<T> = Loading(true)
        fun <T> success(data: T): Resource<T> = Success(data)
        fun <T> error(error: android.app.submission1jetpack.data.base.Error): Resource<T> = Error(error)
        fun <T> init() : Resource<T> = Init(true)
        fun <T> empty(data : T) : Resource<T> = Empty(data)
    }
}