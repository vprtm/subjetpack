package android.app.submission1jetpack.utils

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.util.*

class IntegerListConverter {
    private val gson = Gson()

    @TypeConverter
    fun stringToList(data : String?) : List<Int>? {
        if (data == null) {
            return Collections.emptyList()
        }

        val listType = object : TypeToken<List<Int>>(){}.type

        return gson.fromJson(data, listType)
    }

    @TypeConverter
    fun listToString(someObject : List<Int>?) : String {
        return gson.toJson(someObject)
    }
}