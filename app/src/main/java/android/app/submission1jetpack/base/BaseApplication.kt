package android.app.submission1jetpack.base

import android.app.Application
import android.app.submission1jetpack.di.*
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.module.Module

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@BaseApplication)
            modules(getDefinedModules())
        }

        RxJavaPlugins.setErrorHandler{}
    }

    private fun getDefinedModules(): List<Module> {
        return listOf(
            applicationModule,
            dbModule,
            catalogueModule
        )
    }
}