package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.Resource
import android.app.submission1jetpack.utils.Resource.Companion.init
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import kotlinx.coroutines.launch

class TvShowViewModel(
    private val repository: CatalogueRepository
) : ViewModel() {

    private val _tvShows = MutableLiveData<Resource<List<TvShow>>>()
    val tvShows: LiveData<Resource<List<TvShow>>> by lazy { _tvShows }

    init {
        _tvShows.value = init()
    }

    fun getTvShows(language: String = "en-US", page: Int = 1) {
        viewModelScope.launch {
            _tvShows.value = Resource.loading()
            val data = repository.getRemoteListTvShow(language, page)
            _tvShows.value = data
        }
    }

    fun getAllFavoriteTvShows(): LiveData<PagedList<TvShow>> = repository.getAllFavoriteTvShows()

}