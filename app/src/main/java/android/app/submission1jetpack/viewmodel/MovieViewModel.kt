package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.utils.Resource
import android.app.submission1jetpack.utils.Resource.Companion.init
import android.app.submission1jetpack.utils.Resource.Companion.loading
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagedList
import kotlinx.coroutines.launch

class MovieViewModel(
    private val repository: CatalogueRepository
) : ViewModel() {

    private val _movies = MutableLiveData<Resource<List<Movie>>>()
    val movies: LiveData<Resource<List<Movie>>> by lazy { _movies }

    init {
        _movies.value = init()
    }

    fun getMovies(language: String = "en-US", page: Int = 1) {
        viewModelScope.launch {
            _movies.value = loading()
            val data = repository.getRemoteListMovie(language, page)
            _movies.value = data
        }
    }

    fun getAllFavoriteMovies(): LiveData<PagedList<Movie>> = repository.getAllFavoriteMovies()
}