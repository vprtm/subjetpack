package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.utils.Resource
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class TvShowDetailViewModel(
    private val repository: CatalogueRepository
) : ViewModel() {

    private val _tvShowDetail = MutableLiveData<Resource<TvShow>>()
    val tvShowDetail: LiveData<Resource<TvShow>> by lazy { _tvShowDetail }

    private val _addFavorite = MutableLiveData<Resource<TvShow>>()
    val addFavorite: LiveData<Resource<TvShow>> by lazy { _addFavorite }

    private val _deleteFavorite = MutableLiveData<Resource<TvShow>>()
    val deleteFavorite: LiveData<Resource<TvShow>> by lazy { _deleteFavorite }

    private val _isFavorite = MutableLiveData<Resource<Boolean>>()
    val isFavorite: LiveData<Resource<Boolean>> by lazy { _isFavorite }

    init {
        _tvShowDetail.value = Resource.init()
        _addFavorite.value = Resource.init()
        _deleteFavorite.value = Resource.init()
        _isFavorite.value = Resource.init()
    }

    private var tvShowId = 0

    fun setTvShowId(tvShowId: Int) {
        this.tvShowId = tvShowId
    }

    fun getTvShowDetail(language: String = "en-US") {
        viewModelScope.launch {
            _tvShowDetail.value = Resource.loading()
            val data = repository.getRemoteDetailTvShow(language, tvShowId)
            _tvShowDetail.value = data
        }
    }

    fun addToFavorite(tvShow: TvShow) {
        viewModelScope.launch {
            _addFavorite.value = Resource.loading()
            val data = repository.insertFavoriteTvShow(tvShow)
            _addFavorite.value = data
        }
    }

    fun deleteFromFavorite(tvShow: TvShow) {
        viewModelScope.launch {
            _deleteFavorite.value = Resource.loading()
            val data = repository.deleteFavoriteTvShow(tvShow)
            _deleteFavorite.value = data
        }
    }

    fun getFavorite(id: Int) {
        viewModelScope.launch {
            _isFavorite.value = Resource.loading()
            val data = repository.getFavoriteTvShow(id)
            _isFavorite.value = data
        }
    }
}