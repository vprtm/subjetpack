package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.utils.Resource
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch

class MovieDetailViewModel(
    private val repository: CatalogueRepository
) : ViewModel() {

    private val _movieDetail = MutableLiveData<Resource<Movie>>()
    val movieDetail: LiveData<Resource<Movie>> by lazy { _movieDetail }

    private val _addFavorite = MutableLiveData<Resource<Movie>>()
    val addFavorite: LiveData<Resource<Movie>> by lazy { _addFavorite }

    private val _deleteFavorite = MutableLiveData<Resource<Movie>>()
    val deleteFavorite: LiveData<Resource<Movie>> by lazy { _deleteFavorite }

    private val _isFavorite = MutableLiveData<Resource<Boolean>>()
    val isFavorite: LiveData<Resource<Boolean>> by lazy { _isFavorite }

    init {
        _movieDetail.value = Resource.init()
        _addFavorite.value = Resource.init()
        _deleteFavorite.value = Resource.init()
        _isFavorite.value = Resource.init()
    }

    private var movieId = 0

    fun setMovieId(movieId: Int) {
        this.movieId = movieId
    }

    fun getMovieDetail(language: String = "en-US") {
        viewModelScope.launch {
            _movieDetail.value = Resource.loading()
            val data = repository.getRemoteDetailMovie(language, movieId)
            _movieDetail.value = data
        }
    }

    fun addToFavorite(movie: Movie) {
        viewModelScope.launch {
            _addFavorite.value = Resource.loading()
            val data = repository.insertFavoriteMovie(movie)
            _addFavorite.value = data
        }
    }

    fun deleteFromFavorite(movie: Movie) {
        viewModelScope.launch {
            _deleteFavorite.value = Resource.loading()
            val data = repository.deleteFavoriteMovie(movie)
            _deleteFavorite.value = data
        }
    }

    fun getFavorite(id: Int) {
        viewModelScope.launch {
            _isFavorite.value = Resource.loading()
            val data = repository.getFavoriteMovie(id)
            _isFavorite.value = data
        }
    }
}