package android.app.submission1jetpack.di

import android.app.submission1jetpack.data.base.OkHttpClientService
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val applicationModule = module {

    single {
        return@single OkHttpClientService.create(androidContext())
    }
}