package android.app.submission1jetpack.di

import android.app.submission1jetpack.data.api.MovieDbApi
import android.app.submission1jetpack.data.base.RetrofitService
import android.app.submission1jetpack.data.repo.CatalogueRepositoryImpl
import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.ApiSource
import android.app.submission1jetpack.data.source.LocalSource
import android.app.submission1jetpack.viewmodel.MovieDetailViewModel
import android.app.submission1jetpack.viewmodel.MovieViewModel
import android.app.submission1jetpack.viewmodel.TvShowDetailViewModel
import android.app.submission1jetpack.viewmodel.TvShowViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val catalogueModule = module {

    single {
        RetrofitService.createService(
            MovieDbApi::class.java,
            get()
        )
    }

    single { ApiSource(get()) }

    single { LocalSource(get(), get()) }

    single<CatalogueRepository> {
        CatalogueRepositoryImpl(
            local = get(),
            api = get()
        )
    }

    viewModel { MovieViewModel(get()) }
    viewModel { TvShowViewModel(get()) }
    viewModel { TvShowDetailViewModel(get()) }
    viewModel { MovieDetailViewModel(get()) }
}