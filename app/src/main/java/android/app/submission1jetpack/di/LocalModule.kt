package android.app.submission1jetpack.di

import android.app.submission1jetpack.data.base.AppDatabase
import org.koin.dsl.module

const val DATABASE_NAME = "movie_catalogue_db"

val dbModule = module {

    single { AppDatabase.getDatabase(get(), DATABASE_NAME) }

    single {
        val appDatabase: AppDatabase = get()
        return@single appDatabase.movieDao()
    }

    single {
        val appDatabase: AppDatabase = get()
        return@single appDatabase.tvShowDao()
    }
}