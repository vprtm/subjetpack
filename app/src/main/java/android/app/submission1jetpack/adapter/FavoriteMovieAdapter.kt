package android.app.submission1jetpack.adapter

import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.databinding.ItemListBinding
import android.app.submission1jetpack.ui.DetailActivity
import android.app.submission1jetpack.utils.imageBaseUrl
import android.app.submission1jetpack.utils.setImageUrl
import android.app.submission1jetpack.utils.toViewDate
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

class FavoriteMovieAdapter(private val context: Context) : PagedListAdapter<Movie, FavoriteMovieAdapter.FavoriteMovieViewHolder>(DIFF_CALLBACK) {
    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteMovieViewHolder {
        val itemsBookmarkBinding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FavoriteMovieViewHolder(itemsBookmarkBinding)
    }

    override fun onBindViewHolder(holder: FavoriteMovieViewHolder, position: Int) {
        val course = getItem(position)
        if (course != null) {
            holder.bind(course)
        }
    }

    inner class FavoriteMovieViewHolder(private val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: Movie) {
            with(binding){
                tvName.text = movie.title
                tvOverview.text = movie.overview
                tvDate.text = movie.release_date.toViewDate()
                tvRate.text = movie.vote_average.toString()
                imgPoster.setImageUrl(context, imageBaseUrl() + movie.poster_path, pbPoster)

                root.setOnClickListener {
                    DetailActivity.startMovieDetail(context, movie.id)
                }
            }
        }
    }
}