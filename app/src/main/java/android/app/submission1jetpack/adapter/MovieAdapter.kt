package android.app.submission1jetpack.adapter

import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.databinding.ItemListBinding
import android.app.submission1jetpack.ui.DetailActivity
import android.app.submission1jetpack.utils.imageBaseUrl
import android.app.submission1jetpack.utils.setImageUrl
import android.app.submission1jetpack.utils.toViewDate
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class MovieAdapter(
    private val context: Context,
    private val data: MutableList<Movie> = mutableListOf()
) : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val itemListBinding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MovieViewHolder(itemListBinding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class MovieViewHolder(private val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(items: Movie) {
            with(binding){
                tvName.text = items.title
                tvOverview.text = items.overview
                tvDate.text = items.release_date.toViewDate()
                tvRate.text = items.vote_average.toString()
                imgPoster.setImageUrl(context, imageBaseUrl() + items.poster_path, pbPoster)

                root.setOnClickListener {
                    DetailActivity.startMovieDetail(context, items.id)
                }
            }
        }
    }
}