package android.app.submission1jetpack.adapter

import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.databinding.ItemListBinding
import android.app.submission1jetpack.ui.DetailActivity
import android.app.submission1jetpack.utils.imageBaseUrl
import android.app.submission1jetpack.utils.setImageUrl
import android.app.submission1jetpack.utils.toViewDate
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

class FavoriteTvShowAdapter(private val context: Context) : PagedListAdapter<TvShow, FavoriteTvShowAdapter.FavoriteTvShowViewHolder>(DIFF_CALLBACK) {
    companion object {
        private val DIFF_CALLBACK = object : DiffUtil.ItemCallback<TvShow>() {
            override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
                return oldItem == newItem
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteTvShowViewHolder {
        val itemsBookmarkBinding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FavoriteTvShowViewHolder(itemsBookmarkBinding)
    }

    override fun onBindViewHolder(holder: FavoriteTvShowViewHolder, position: Int) {
        val course = getItem(position)
        if (course != null) {
            holder.bind(course)
        }
    }

    inner class FavoriteTvShowViewHolder(private val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(tvShow: TvShow) {
            with(binding){
                tvName.text = tvShow.name
                tvOverview.text = tvShow.overview
                tvDate.text = tvShow.first_air_date.toViewDate()
                tvRate.text = tvShow.vote_average.toString()
                imgPoster.setImageUrl(context, imageBaseUrl() + tvShow.poster_path, pbPoster)

                root.setOnClickListener {
                    DetailActivity.startTvShowDetail(context, tvShow.id)
                }
            }
        }
    }
}