package android.app.submission1jetpack.adapter

import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.databinding.ItemListBinding
import android.app.submission1jetpack.ui.DetailActivity
import android.app.submission1jetpack.utils.imageBaseUrl
import android.app.submission1jetpack.utils.setImageUrl
import android.app.submission1jetpack.utils.toViewDate
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class TvShowAdapter(
    private val context: Context,
    private val data: MutableList<TvShow> = mutableListOf()
) : RecyclerView.Adapter<TvShowAdapter.TvShowViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowViewHolder {
        val itemListBinding = ItemListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return TvShowViewHolder(itemListBinding)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: TvShowViewHolder, position: Int) {
        holder.bind(data[position])
    }

    inner class TvShowViewHolder(private val binding: ItemListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(items: TvShow) {
            with(binding){
                tvName.text = items.name
                tvOverview.text = items.overview
                tvDate.text = items.first_air_date.toViewDate()
                tvRate.text = items.vote_average.toString()
                imgPoster.setImageUrl(context, imageBaseUrl() + items.poster_path, pbPoster)

                root.setOnClickListener {
                    DetailActivity.startTvShowDetail(context, items.id)
                }
            }
        }
    }
}