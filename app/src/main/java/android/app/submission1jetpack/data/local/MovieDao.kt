package android.app.submission1jetpack.data.local

import android.app.submission1jetpack.data.local.model.FavoriteMovieEntity
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFavoriteMovie(movie: FavoriteMovieEntity)

    @Query("SELECT * from favorite_movie WHERE id = :id")
    fun getFavoriteMovie(id : Int) : FavoriteMovieEntity?

    @Delete
    fun deleteFavoriteMovie(movie: FavoriteMovieEntity)

    @Query("SELECT * from favorite_movie ORDER BY id ASC")
    fun getAllFavoriteMovies(): DataSource.Factory<Int, FavoriteMovieEntity>

}