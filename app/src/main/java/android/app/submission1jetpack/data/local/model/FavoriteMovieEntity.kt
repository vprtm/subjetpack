package android.app.submission1jetpack.data.local.model

import android.app.submission1jetpack.data.source.model.Movie
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "favorite_movie")
@Parcelize
data class FavoriteMovieEntity(
    @PrimaryKey
    val id: Int,
    val adult: Boolean,
    val backdrop_path: String,
    val genre_ids: List<Int>,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String,
    val release_date: String,
    val title: String,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Int
) : Parcelable {

    fun toMovie() : Movie {
        return Movie(
            id = id,
            adult = adult,
            backdrop_path = backdrop_path,
            genre_ids = genre_ids,
            title = title,
            original_language = original_language,
            original_title = original_title,
            overview = overview,
            popularity = popularity,
            poster_path = poster_path,
            release_date = release_date,
            video = video,
            vote_average = vote_average,
            vote_count = vote_count
        )
    }
}