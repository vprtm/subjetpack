package android.app.submission1jetpack.data.repo

import android.app.submission1jetpack.BuildConfig
import android.app.submission1jetpack.data.local.model.FavoriteMovieEntity
import android.app.submission1jetpack.data.local.model.FavoriteTvShowEntity
import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.ApiSource
import android.app.submission1jetpack.data.source.LocalSource
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.Resource
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList

class CatalogueRepositoryImpl(private val api: ApiSource, private val local: LocalSource): CatalogueRepository {

    private val apiKey = BuildConfig.MOVIE_DB_TOKEN

    override suspend fun getRemoteDetailMovie(language: String, id: Int): Resource<Movie> {
        return api.getDetailMovieFromApi(apiKey = apiKey, movieId = id, language = language)
    }

    override suspend fun getRemoteListMovie(language: String, page: Int): Resource<List<Movie>> {
        return api.getListMovieFromApi(apiKey = apiKey, language = language, page = page)
    }

    override suspend fun getRemoteDetailTvShow(language: String, id: Int): Resource<TvShow> {
        return api.getDetailTvShowFromApi(apiKey = apiKey, tvId = id, language = language)
    }

    override suspend fun getRemoteListTvShow(language: String, page: Int): Resource<List<TvShow>> {
        return api.getListTvShowFromApi(apiKey = apiKey, language = language, page = page)
    }

    override fun insertFavoriteMovie(movie: Movie): Resource<Movie> {
        return local.insertFavoriteMovie(movie)
    }

    override fun insertFavoriteTvShow(tvShow: TvShow): Resource<TvShow> {
        return local.insertFavoriteTvShow(tvShow)
    }

    override fun deleteFavoriteMovie(movie: Movie): Resource<Movie> {
        return local.deleteFavoriteMovie(movie)
    }

    override fun deleteFavoriteTvShow(tvShow: TvShow): Resource<TvShow> {
        return local.deleteFavoriteTvShow(tvShow)
    }

    override fun getAllFavoriteMovies(): LiveData<PagedList<Movie>> {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(4)
            .setPageSize(4)
            .build()
        return LivePagedListBuilder(local.getLocalListMovie(), config).build()
    }

    override fun getAllFavoriteTvShows(): LiveData<PagedList<TvShow>> {
        val config = PagedList.Config.Builder()
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(4)
            .setPageSize(4)
            .build()
        return LivePagedListBuilder(local.getLocalListTvShow(), config).build()
    }

    override suspend fun getFavoriteMovie(id : Int): Resource<Boolean> {
        return local.getFavoriteMovie(id)
    }

    override suspend fun getFavoriteTvShow(id : Int): Resource<Boolean> {
        return local.getFavoriteTvShow(id)
    }
}