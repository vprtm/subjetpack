package android.app.submission1jetpack.data.local

import android.app.submission1jetpack.data.local.model.FavoriteTvShowEntity
import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.*

@Dao
interface TvShowDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertFavoriteTvShow(tvShow: FavoriteTvShowEntity)

    @Query("SELECT * from favorite_tv_show WHERE id = :id")
    fun getFavoriteTvShow(id : Int) : FavoriteTvShowEntity?

    @Delete
    fun deleteFavoriteTvShow(tvShow: FavoriteTvShowEntity)

    @Query("SELECT * from favorite_tv_show ORDER BY id ASC")
    fun getAllFavoriteTvShows(): DataSource.Factory<Int, FavoriteTvShowEntity>
}