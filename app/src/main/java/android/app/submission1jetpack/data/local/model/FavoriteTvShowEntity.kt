package android.app.submission1jetpack.data.local.model

import android.app.submission1jetpack.data.source.model.TvShow
import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "favorite_tv_show")
@Parcelize
data class FavoriteTvShowEntity(
    @PrimaryKey
    val id: Int,
    val backdrop_path: String,
    val first_air_date: String,
    val genre_ids: List<Int>,
    val name: String = "",
    val origin_country: List<String>,
    val original_language: String,
    val original_name: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String,
    val vote_average: Double,
    val vote_count: Int
) : Parcelable {

    fun toTvShow(): TvShow {
        return TvShow(
            id = id,
            vote_count = vote_count,
            vote_average = vote_average,
            poster_path = poster_path,
            popularity = popularity,
            overview = overview,
            original_language = original_language,
            name = name,
            backdrop_path = backdrop_path,
            first_air_date = first_air_date,
            genre_ids = genre_ids,
            origin_country = origin_country,
            original_name = original_name
        )
    }
}