package android.app.submission1jetpack.data.api.model

import android.app.submission1jetpack.data.source.model.TvShow
import com.google.gson.annotations.SerializedName

data class TvShowResponse(
    @SerializedName("backdrop_path")
    val backdrop_path: String?,
    @SerializedName("first_air_date")
    val first_air_date: String?,
    @SerializedName("genre_ids")
    val genre_ids: List<Int>?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("name")
    val name: String?,
    @SerializedName("origin_country")
    val origin_country: List<String>?,
    @SerializedName("original_language")
    val original_language: String?,
    @SerializedName("original_name")
    val original_name: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("popularity")
    val popularity: Double?,
    @SerializedName("poster_path")
    val poster_path: String?,
    @SerializedName("vote_average")
    val vote_average: Double?,
    @SerializedName("vote_count")
    val vote_count: Int?
) {
    fun toTvShow(): TvShow {
        return TvShow(
            id = id ?: 0,
            vote_count = vote_count ?: 0,
            vote_average = vote_average ?: 0.0,
            poster_path = poster_path.orEmpty(),
            popularity = popularity ?: 0.0,
            overview = overview.orEmpty(),
            original_language = original_language.orEmpty(),
            name = name.orEmpty(),
            backdrop_path = backdrop_path.orEmpty(),
            first_air_date = first_air_date.orEmpty(),
            genre_ids = genre_ids.orEmpty(),
            origin_country = origin_country.orEmpty(),
            original_name = original_name.orEmpty()
        )
    }
}