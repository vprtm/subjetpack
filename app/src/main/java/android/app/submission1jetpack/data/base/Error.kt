package android.app.submission1jetpack.data.base

import com.google.gson.annotations.SerializedName

data class Error(
    @SerializedName("success")
    var status: Boolean = false,
    @SerializedName("status_message")
    var message: String = "",
    @SerializedName("status_code")
    var code: Int = 0
)