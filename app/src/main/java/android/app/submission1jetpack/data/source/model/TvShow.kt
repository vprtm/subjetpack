package android.app.submission1jetpack.data.source.model

import android.app.submission1jetpack.data.local.model.FavoriteTvShowEntity

data class TvShow(
    val backdrop_path: String = "",
    val first_air_date: String = "",
    val genre_ids: List<Int> = listOf(),
    val id: Int = 0,
    val name: String = "",
    val origin_country: List<String> = listOf(),
    val original_language: String = "",
    val original_name: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    val poster_path: String = "",
    val vote_average: Double = 0.0,
    val vote_count: Int = 0
) {

    fun toTvShowFavoriteEntity(): FavoriteTvShowEntity {
        return FavoriteTvShowEntity(
            id = id,
            vote_count = vote_count,
            vote_average = vote_average,
            poster_path = poster_path,
            popularity = popularity,
            overview = overview,
            original_language = original_language,
            name = name,
            backdrop_path = backdrop_path,
            first_air_date = first_air_date,
            genre_ids = genre_ids,
            origin_country = origin_country,
            original_name = original_name
        )
    }
}
