package android.app.submission1jetpack.data.api

import android.app.submission1jetpack.data.api.model.MovieResponse
import android.app.submission1jetpack.data.api.model.TvShowResponse
import android.app.submission1jetpack.data.base.ApiResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieDbApi {

    @GET("movie/now_playing")
    suspend fun getNowPlayings(
        @Query("api_key") apiKey : String,
        @Query("language") language : String,
        @Query("page") page : Int
    ): Response<ApiResponse<List<MovieResponse>>>

    @GET("movie/{movieId}")
    suspend fun getDetailMovie(
        @Path("movieId") movieId : Int,
        @Query("api_key") apiKey : String,
        @Query("language") language : String
    ): Response<MovieResponse>

    @GET("tv/airing_today")
    suspend fun getAiringsToday(
        @Query("api_key") apiKey : String,
        @Query("language") language : String,
        @Query("page") page : Int
    ): Response<ApiResponse<List<TvShowResponse>>>

    @GET("tv/{tvId}}")
    suspend fun getDetailTvShow(
        @Path("tvId") tvId : Int,
        @Query("api_key") apiKey : String,
        @Query("language") language : String
    ): Response<TvShowResponse>

}