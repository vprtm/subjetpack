package android.app.submission1jetpack.data.base

import android.util.Log
import com.google.gson.GsonBuilder
import retrofit2.Response
import java.io.IOException

object ApiErrorOperator {

    fun parseError(response: Response<*>?): Error {
        val gson = GsonBuilder().create()
        val error: Error

        try {
            error = gson.fromJson(response?.errorBody()?.string(), Error::class.java)
        } catch (e: IOException) {
            e.message?.let { Log.d("error", it) }
            return Error(message = e.message.toString())
        }
        return error
    }

}