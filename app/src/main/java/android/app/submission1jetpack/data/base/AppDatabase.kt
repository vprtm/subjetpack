package android.app.submission1jetpack.data.base

import android.app.submission1jetpack.data.local.MovieDao
import android.app.submission1jetpack.data.local.TvShowDao
import android.app.submission1jetpack.data.local.model.FavoriteMovieEntity
import android.app.submission1jetpack.data.local.model.FavoriteTvShowEntity
import android.app.submission1jetpack.utils.IntegerListConverter
import android.app.submission1jetpack.utils.StringListConverter
import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(
    entities = [
        FavoriteTvShowEntity::class,
        FavoriteMovieEntity::class
    ], version = 1, exportSchema = false
)
@TypeConverters(value = [IntegerListConverter::class, StringListConverter::class])
abstract class AppDatabase : RoomDatabase() {

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getDatabase(
            context: Context,
            databaseName: String,
        ): AppDatabase {
            if (INSTANCE == null) {
                val builder = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    databaseName
                ).fallbackToDestructiveMigration()
                INSTANCE = builder.build()
            }
            return INSTANCE!!
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

    abstract fun tvShowDao(): TvShowDao

    abstract fun movieDao(): MovieDao
}