package android.app.submission1jetpack.data.api.model

import android.app.submission1jetpack.data.source.model.Movie
import com.google.gson.annotations.SerializedName

data class MovieResponse(
    @SerializedName("adult")
    val adult: Boolean?,
    @SerializedName("backdrop_path")
    val backdrop_path: String?,
    @SerializedName("genre_ids")
    val genre_ids: List<Int>?,
    @SerializedName("id")
    val id: Int?,
    @SerializedName("original_language")
    val original_language: String?,
    @SerializedName("original_title")
    val original_title: String?,
    @SerializedName("overview")
    val overview: String?,
    @SerializedName("popularity")
    val popularity: Double?,
    @SerializedName("poster_path")
    val poster_path: String?,
    @SerializedName("release_date")
    val release_date: String?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("video")
    val video: Boolean?,
    @SerializedName("vote_average")
    val vote_average: Double?,
    @SerializedName("vote_count")
    val vote_count: Int?
){
    fun toMovie() : Movie {
        return Movie(
            id = id ?: 0,
            adult = adult ?: false,
            backdrop_path = backdrop_path.orEmpty(),
            genre_ids = genre_ids.orEmpty(),
            title = title.orEmpty(),
            original_language = original_language.orEmpty(),
            original_title = original_title.orEmpty(),
            overview = overview.orEmpty(),
            popularity = popularity ?: 0.0,
            poster_path = poster_path.orEmpty(),
            release_date = release_date.orEmpty(),
            video = video ?: false,
            vote_average = vote_average ?: 0.0,
            vote_count = vote_count ?: 0
        )
    }
}