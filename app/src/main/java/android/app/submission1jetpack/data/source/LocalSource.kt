package android.app.submission1jetpack.data.source

import android.app.submission1jetpack.data.base.Error
import android.app.submission1jetpack.data.local.MovieDao
import android.app.submission1jetpack.data.local.TvShowDao
import android.app.submission1jetpack.data.local.model.FavoriteMovieEntity
import android.app.submission1jetpack.data.local.model.FavoriteTvShowEntity
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.Resource
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class LocalSource(private val movieDao: MovieDao, private val tvShowDao: TvShowDao) {

    private val executorService: ExecutorService = Executors.newSingleThreadExecutor()

    fun getLocalListMovie(): DataSource.Factory<Int, Movie> {
        return movieDao.getAllFavoriteMovies().map {
            it.toMovie()
        }
    }

    fun getLocalListTvShow(): DataSource.Factory<Int, TvShow> {
        return tvShowDao.getAllFavoriteTvShows().map {
            it.toTvShow()
        }
    }

    fun insertFavoriteMovie(movie: Movie): Resource<Movie> {
        return try {
            executorService.execute {
                movieDao.insertFavoriteMovie(movie.toMovieFavoriteEntity())
            }
            Resource.success(movie)
        } catch (e: Exception) {
            Resource.error(Error(message = e.message.toString()))
        }
    }

    fun insertFavoriteTvShow(tvShow: TvShow): Resource<TvShow> {
        return try {
            executorService.execute {
                tvShowDao.insertFavoriteTvShow(tvShow.toTvShowFavoriteEntity())
            }
            Resource.success(tvShow)
        } catch (e: Exception) {
            Resource.error(Error(message = e.message.toString()))
        }
    }

    fun deleteFavoriteMovie(movie: Movie): Resource<Movie> {
        return try {
            executorService.execute {
                movieDao.deleteFavoriteMovie(movie.toMovieFavoriteEntity())
            }
            Resource.success(movie)
        } catch (e: Exception) {
            Resource.error(Error(message = e.message.toString()))
        }
    }

    fun deleteFavoriteTvShow(tvShow: TvShow): Resource<TvShow> {
        return try {
            executorService.execute {
                tvShowDao.deleteFavoriteTvShow(tvShow.toTvShowFavoriteEntity())
            }
            Resource.success(tvShow)
        } catch (e: Exception) {
            Resource.error(Error(message = e.message.toString()))
        }
    }

    suspend fun getFavoriteMovie(id: Int): Resource<Boolean> {
        return try {
            withContext(Dispatchers.IO){
                Resource.success(movieDao.getFavoriteMovie(id) != null)
            }
        }
        catch (e: Exception) {
            Resource.error(Error(message = e.message.toString()))
        }
    }

    suspend fun getFavoriteTvShow(id: Int): Resource<Boolean> {
        return try {
            withContext(Dispatchers.IO){
                Resource.success(tvShowDao.getFavoriteTvShow(id) != null)
            }
        }
        catch (e: Exception) {
            Resource.error(Error(message = e.message.toString()))
        }
    }
}