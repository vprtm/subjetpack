package android.app.submission1jetpack.data.repo.helper

import android.app.submission1jetpack.data.local.model.FavoriteMovieEntity
import android.app.submission1jetpack.data.local.model.FavoriteTvShowEntity
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.Resource
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import io.reactivex.Completable

interface CatalogueRepository {
    suspend fun getRemoteDetailMovie(language : String = "en-US", id : Int): Resource<Movie>
    suspend fun getRemoteListMovie(language : String = "en-US", page : Int = 1): Resource<List<Movie>>
    suspend fun getRemoteDetailTvShow(language : String = "en-US", id : Int): Resource<TvShow>
    suspend fun getRemoteListTvShow(language : String = "en-US", page : Int = 1): Resource<List<TvShow>>

    fun insertFavoriteMovie(movie: Movie) : Resource<Movie>
    fun insertFavoriteTvShow(tvShow: TvShow) : Resource<TvShow>

    fun deleteFavoriteMovie(movie: Movie) : Resource<Movie>
    fun deleteFavoriteTvShow(tvShow: TvShow) : Resource<TvShow>

    fun getAllFavoriteMovies() : LiveData<PagedList<Movie>>
    fun getAllFavoriteTvShows() : LiveData<PagedList<TvShow>>

    suspend fun getFavoriteMovie(id : Int) : Resource<Boolean>
    suspend fun getFavoriteTvShow(id : Int) : Resource<Boolean>
}