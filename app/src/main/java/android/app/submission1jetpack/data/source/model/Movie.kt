package android.app.submission1jetpack.data.source.model

import android.app.submission1jetpack.data.local.model.FavoriteMovieEntity

data class Movie(
    val adult: Boolean = false,
    val backdrop_path: String = "",
    val genre_ids: List<Int> = listOf(),
    val id: Int = 0,
    val original_language: String = "",
    val original_title: String = "",
    val overview: String = "",
    val popularity: Double = 0.0,
    val poster_path: String = "",
    val release_date: String = "",
    val title: String = "",
    val video: Boolean = false,
    val vote_average: Double = 0.0,
    val vote_count: Int = 0
){

    fun toMovieFavoriteEntity() : FavoriteMovieEntity {
        return FavoriteMovieEntity(
            id = id,
            adult = adult,
            backdrop_path = backdrop_path,
            genre_ids = genre_ids,
            title = title,
            original_language = original_language,
            original_title = original_title,
            overview = overview,
            popularity = popularity,
            poster_path = poster_path,
            release_date = release_date,
            video = video,
            vote_average = vote_average,
            vote_count = vote_count
        )
    }
}
