package android.app.submission1jetpack.data.source

import android.app.submission1jetpack.BuildConfig
import android.app.submission1jetpack.data.api.MovieDbApi
import android.app.submission1jetpack.data.base.Error
import android.app.submission1jetpack.data.base.ApiErrorOperator
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.EspressoIdlingResource
import android.app.submission1jetpack.utils.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ApiSource(private val apiService: MovieDbApi) {

    suspend fun getDetailMovieFromApi(
        movieId: Int,
        apiKey: String = BuildConfig.MOVIE_DB_TOKEN,
        language: String = "en-US"
    ): Resource<Movie> {
        EspressoIdlingResource.increment()
        return try {
            val response = apiService.getDetailMovie(movieId, apiKey, language)
            if (response.isSuccessful) {
                EspressoIdlingResource.decrement()
                val body = response.body()
                body?.toMovie()?.let {
                    withContext(Dispatchers.IO) {
                        Resource.success(it)
                    }
                } ?: Resource.Empty(Movie())
            } else {
                EspressoIdlingResource.decrement()
                Resource.error(ApiErrorOperator.parseError(response))
            }
        } catch (e: Exception) {
            EspressoIdlingResource.decrement()
            Resource.error(Error(message = e.message.toString()))
        }
    }

    suspend fun getListMovieFromApi(
        apiKey: String = BuildConfig.MOVIE_DB_TOKEN,
        language: String = "en-US",
        page: Int = 1
    ): Resource<List<Movie>> {
        EspressoIdlingResource.increment()
        return try {
            val response = apiService.getNowPlayings(apiKey, language, page)
            if (response.isSuccessful) {
                EspressoIdlingResource.decrement()
                val body = response.body()
                body?.results?.let {
                    withContext(Dispatchers.IO) {
                        Resource.success(it.map { it.toMovie() })
                    }
                } ?: Resource.Empty(listOf())
            } else {
                EspressoIdlingResource.decrement()
                Resource.error(ApiErrorOperator.parseError(response))
            }
        } catch (e: Exception) {
            EspressoIdlingResource.decrement()
            Resource.error(Error(message = e.message.toString()))
        }
    }

    suspend fun getDetailTvShowFromApi(
        tvId: Int,
        apiKey: String = BuildConfig.MOVIE_DB_TOKEN,
        language: String = "en-US"
    ): Resource<TvShow> {
        EspressoIdlingResource.increment()
        return try {
            val response = apiService.getDetailTvShow(tvId, apiKey, language)
            if (response.isSuccessful) {
                EspressoIdlingResource.decrement()
                val body = response.body()
                body?.toTvShow()?.let {
                    withContext(Dispatchers.IO) {
                        Resource.success(it)
                    }
                } ?: Resource.Empty(TvShow())
            } else {
                EspressoIdlingResource.decrement()
                Resource.error(ApiErrorOperator.parseError(response))
            }
        } catch (e: Exception) {
            EspressoIdlingResource.decrement()
            Resource.error(Error(message = e.message.toString()))
        }
    }

    suspend fun getListTvShowFromApi(
        apiKey: String = BuildConfig.MOVIE_DB_TOKEN,
        language: String = "en-US",
        page: Int = 1
    ): Resource<List<TvShow>> {
        EspressoIdlingResource.increment()
        return try {
            val response = apiService.getAiringsToday(apiKey,language,page)
            if (response.isSuccessful) {
                EspressoIdlingResource.decrement()
                val body = response.body()
                body?.results?.let {
                    withContext(Dispatchers.IO) {
                        Resource.success(it.map { it.toTvShow() })
                    }
                } ?: Resource.Empty(listOf())
            } else {
                EspressoIdlingResource.decrement()
                Resource.error(ApiErrorOperator.parseError(response))
            }
        } catch (e: Exception) {
            EspressoIdlingResource.decrement()
            Resource.error(Error(message = e.message.toString()))
        }
    }
}