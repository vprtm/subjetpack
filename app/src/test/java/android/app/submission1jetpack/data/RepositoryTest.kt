package android.app.submission1jetpack.data

import android.app.submission1jetpack.data.repo.CatalogueRepositoryImpl
import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.ApiSource
import android.app.submission1jetpack.data.source.LocalSource
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.DataDummy
import android.app.submission1jetpack.utils.PagedListUtils
import android.app.submission1jetpack.utils.Resource
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.DataSource
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.Mockito.mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner.Silent::class)
class RepositoryTest : KoinTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var rule = MockitoJUnit.rule().silent()

    private val testDispatcher = TestCoroutineDispatcher()

    private val repository by inject<CatalogueRepository>()

    private val dummyMovie = DataDummy.generateMovie()

    private val dummyTvShow = DataDummy.generateTvShow()

    @Mock
    private lateinit var apiSource: ApiSource

    @Mock
    private lateinit var localSource: LocalSource

    @Before
    fun setUp() {
        Dispatchers.setMain(testDispatcher)

        startKoin {
            modules(
                module {
                    single<CatalogueRepository> {
                        CatalogueRepositoryImpl(apiSource, localSource)
                    }
                }
            )
        }
    }

    @After
    fun tearDown() {
        stopKoin()
        Dispatchers.resetMain()
    }

    @Test
    fun `get movie detail success`() {
        testDispatcher.runBlockingTest {
            val movie = Resource.Success(dummyMovie)
            whenever(apiSource.getDetailMovieFromApi(movieId = dummyMovie.id)).thenReturn(movie)
            val actualMovie = repository.getRemoteDetailMovie(id = dummyMovie.id)
            verify(apiSource).getDetailMovieFromApi(movieId = dummyMovie.id)
            assertEquals(movie, actualMovie)
        }
    }

    @Test
    fun `get movies success`() {
        testDispatcher.runBlockingTest {
            val movies = Resource.Success(DataDummy.generateMovies())
            whenever(apiSource.getListMovieFromApi()).thenReturn(movies)
            val actualMovies = repository.getRemoteListMovie()
            verify(apiSource).getListMovieFromApi()
            assertEquals(movies, actualMovies)
        }
    }

    @Test
    fun `get tv show detail success`() {
        testDispatcher.runBlockingTest {
            val tvShow = Resource.Success(dummyTvShow)
            whenever(apiSource.getDetailTvShowFromApi(tvId = dummyTvShow.id)).thenReturn(tvShow)
            val actualTvShow = repository.getRemoteDetailTvShow(id = dummyTvShow.id)
            verify(apiSource).getDetailTvShowFromApi(tvId = dummyTvShow.id)
            assertEquals(tvShow, actualTvShow)
        }
    }

    @Test
    fun `get tv shows success`() {
        testDispatcher.runBlockingTest {
            val tvShows = Resource.Success(DataDummy.generateTvShows())
            whenever(apiSource.getListTvShowFromApi()).thenReturn(tvShows)
            val actualTvShows = repository.getRemoteListTvShow()
            verify(apiSource).getListTvShowFromApi()
            assertEquals(tvShows, actualTvShows)
        }
    }

    @Test
    fun `get favorite movies`(){
        testDispatcher.runBlockingTest {
            val dataSourceFactory = mock(DataSource.Factory::class.java) as DataSource.Factory<Int, Movie>
            whenever(localSource.getLocalListMovie()).thenReturn(dataSourceFactory)
            repository.getAllFavoriteMovies()
            val movies = PagedListUtils.mockPagedList(DataDummy.generateMovies())
            verify(localSource).getLocalListMovie()
            assertNotNull(movies)
        }
    }

    @Test
    fun `get favorite tv shows`(){
        testDispatcher.runBlockingTest {
            val dataSourceFactory = mock(DataSource.Factory::class.java) as DataSource.Factory<Int, TvShow>
            whenever(localSource.getLocalListTvShow()).thenReturn(dataSourceFactory)
            repository.getAllFavoriteTvShows()
            val tvShows = PagedListUtils.mockPagedList(DataDummy.generateTvShows())
            verify(localSource).getLocalListTvShow()
            assertNotNull(tvShows)
        }
    }

    @Test
    fun `add favorite tv show success`() {
        testDispatcher.runBlockingTest {
            val tvShow = Resource.Success(DataDummy.generateTvShow())
            whenever(localSource.insertFavoriteTvShow(tvShow.data)).thenReturn(tvShow)
            val addTvShow = repository.insertFavoriteTvShow(tvShow.data)
            verify(localSource).insertFavoriteTvShow(tvShow.data)
            assertEquals(tvShow, addTvShow)
        }
    }

    @Test
    fun `add favorite movie success`() {
        testDispatcher.runBlockingTest {
            val movie = Resource.Success(DataDummy.generateMovie())
            whenever(localSource.insertFavoriteMovie(movie.data)).thenReturn(movie)
            val addMovie = repository.insertFavoriteMovie(movie.data)
            verify(localSource).insertFavoriteMovie(movie.data)
            assertEquals(movie, addMovie)
        }
    }

    @Test
    fun `delete favorite tv show success`() {
        testDispatcher.runBlockingTest {
            val tvShow = Resource.Success(DataDummy.generateTvShow())
            whenever(localSource.deleteFavoriteTvShow(tvShow.data)).thenReturn(tvShow)
            val deleteTvShow = repository.deleteFavoriteTvShow(tvShow.data)
            verify(localSource).deleteFavoriteTvShow(tvShow.data)
            assertEquals(tvShow, deleteTvShow)
        }
    }

    @Test
    fun `delete favorite movie success`() {
        testDispatcher.runBlockingTest {
            val movie = Resource.Success(DataDummy.generateMovie())
            whenever(localSource.deleteFavoriteMovie(movie.data)).thenReturn(movie)
            val deleteMovie = repository.deleteFavoriteMovie(movie.data)
            verify(localSource).deleteFavoriteMovie(movie.data)
            assertEquals(movie, deleteMovie)
        }
    }

    @Test
    fun `get favorite movie success`() {
        testDispatcher.runBlockingTest {
            val movie = DataDummy.generateMovie()
            val success = Resource.success(true)
            whenever(localSource.getFavoriteMovie(movie.id)).thenReturn(success)
            val getMovie = repository.getFavoriteMovie(movie.id)
            verify(localSource).getFavoriteMovie(movie.id)
            assertEquals(success, getMovie)
        }
    }

    @Test
    fun `get favorite tv show success`() {
        testDispatcher.runBlockingTest {
            val tvShow = DataDummy.generateTvShow()
            val success = Resource.success(true)
            whenever(localSource.getFavoriteTvShow(tvShow.id)).thenReturn(success)
            val getTvShow = repository.getFavoriteTvShow(tvShow.id)
            verify(localSource).getFavoriteTvShow(tvShow.id)
            assertEquals(success, getTvShow)
        }
    }
}