package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.DataDummy
import android.app.submission1jetpack.utils.Resource
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TvShowViewModelTest : KoinTest {

    private val viewModel by inject<TvShowViewModel>()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var catalogueRepository: CatalogueRepository

    @Mock
    private lateinit var observer: Observer<PagedList<TvShow>>

    @Mock
    private lateinit var pagedList: PagedList<TvShow>

    @Before
    fun setUp() {
        Dispatchers.setMain(TestCoroutineDispatcher())

        startKoin {
            modules(
                module {
                    single {
                        TvShowViewModel(catalogueRepository)
                    }
                }
            )
        }
    }

    @After
    fun tearDown() {
        stopKoin()
        Dispatchers.resetMain()
    }

    @Test
    fun `test get tv shows`() {
        runBlockingTest {
            val tvShows = Resource.Success(DataDummy.generateTvShows())
            whenever(catalogueRepository.getRemoteListTvShow()).thenReturn(tvShows)

            val actual = mutableListOf<Resource<List<TvShow>>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                tvShows
            )

            viewModel.tvShows.observeForever {
                actual.add(it)
            }

            viewModel.getTvShows()

            Mockito.verify(catalogueRepository).getRemoteListTvShow()

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test empty`() {
        runBlockingTest {
            val empty = Resource.empty(listOf<TvShow>())
            whenever(catalogueRepository.getRemoteListTvShow()).thenReturn(empty)

            val actual = mutableListOf<Resource<List<TvShow>>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                empty
            )

            viewModel.tvShows.observeForever {
                actual.add(it)
            }

            viewModel.getTvShows()

            Mockito.verify(catalogueRepository).getRemoteListTvShow()

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test error`() {
        runBlockingTest {
            val error = Resource.Error("error")
            whenever(catalogueRepository.getRemoteListTvShow()).thenReturn(error)

            val actual = mutableListOf<Resource<List<TvShow>>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                error
            )

            viewModel.tvShows.observeForever {
                actual.add(it)
            }

            viewModel.getTvShows()

            Mockito.verify(catalogueRepository).getRemoteListTvShow()

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `get favorite tv shows`() {
        val dummyCourses = pagedList
        whenever(dummyCourses.size).thenReturn(5)
        val tvShows = MutableLiveData<PagedList<TvShow>>()
        tvShows.value = dummyCourses

        whenever(catalogueRepository.getAllFavoriteTvShows()).thenReturn(tvShows)
        val courseEntities = viewModel.getAllFavoriteTvShows().value
        Mockito.verify(catalogueRepository).getAllFavoriteTvShows()
        assertNotNull(courseEntities)
        assertEquals(5, courseEntities?.size)

        viewModel.getAllFavoriteTvShows().observeForever(observer)
        Mockito.verify(observer).onChanged(dummyCourses)
    }
}