package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.utils.DataDummy
import android.app.submission1jetpack.utils.Resource
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MovieDetailViewModelTest : KoinTest {

    private val viewModel by inject<MovieDetailViewModel>()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val dummyMovie = DataDummy.generateMovie()

    @Mock
    private lateinit var catalogueRepository: CatalogueRepository

    @Before
    fun setUp() {
        Dispatchers.setMain(TestCoroutineDispatcher())

        startKoin {
            modules(
                module {
                    single {
                        MovieDetailViewModel(catalogueRepository)
                    }
                }
            )
        }
    }

    @After
    fun tearDown() {
        stopKoin()
        Dispatchers.resetMain()
    }

    @Test
    fun `test get movie detail`() {
        runBlockingTest {
            val movies = Resource.Success(dummyMovie)
            whenever(catalogueRepository.getRemoteDetailMovie(id = dummyMovie.id)).thenReturn(movies)

            val actual = mutableListOf<Resource<Movie>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                movies
            )

            viewModel.movieDetail.observeForever {
                actual.add(it)
            }

            viewModel.setMovieId(dummyMovie.id)

            viewModel.getMovieDetail()

            Mockito.verify(catalogueRepository).getRemoteDetailMovie(id = dummyMovie.id)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test empty`() {
        runBlockingTest {
            val empty = Resource.empty(Movie())
            whenever(catalogueRepository.getRemoteDetailMovie(id = 0)).thenReturn(empty)

            val actual = mutableListOf<Resource<Movie>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                empty
            )

            viewModel.movieDetail.observeForever {
                actual.add(it)
            }

            viewModel.setMovieId(0)

            viewModel.getMovieDetail()

            Mockito.verify(catalogueRepository).getRemoteDetailMovie(id = 0)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test error`() {
        runBlockingTest {
            val error = Resource.Error("error")
            whenever(catalogueRepository.getRemoteDetailMovie(id = 0)).thenReturn(error)

            val actual = mutableListOf<Resource<Movie>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                error
            )

            viewModel.movieDetail.observeForever {
                actual.add(it)
            }

            viewModel.setMovieId(0)

            viewModel.getMovieDetail()

            Mockito.verify(catalogueRepository).getRemoteDetailMovie(id = 0)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test add favorite movie`() {
        runBlockingTest {
            val movies = Resource.Success(dummyMovie)
            whenever(catalogueRepository.insertFavoriteMovie(movies.data)).thenReturn(movies)

            val actual = mutableListOf<Resource<Movie>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                movies
            )

            viewModel.addFavorite.observeForever {
                actual.add(it)
            }

            viewModel.addToFavorite(movies.data)

            Mockito.verify(catalogueRepository).insertFavoriteMovie(movies.data)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test delete favorite movie`() {
        runBlockingTest {
            val movies = Resource.Success(dummyMovie)
            whenever(catalogueRepository.deleteFavoriteMovie(movies.data)).thenReturn(movies)

            val actual = mutableListOf<Resource<Movie>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                movies
            )

            viewModel.deleteFavorite.observeForever {
                actual.add(it)
            }

            viewModel.deleteFromFavorite(movies.data)

            Mockito.verify(catalogueRepository).deleteFavoriteMovie(movies.data)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test get favorite movie`() {
        runBlockingTest {
            val success = Resource.success(true)
            whenever(catalogueRepository.getFavoriteMovie(dummyMovie.id)).thenReturn(success)

            val actual = mutableListOf<Resource<Boolean>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                success
            )

            viewModel.isFavorite.observeForever {
                actual.add(it)
            }

            viewModel.getFavorite(dummyMovie.id)

            Mockito.verify(catalogueRepository).getFavoriteMovie(dummyMovie.id)

            assertEquals(expected, actual)
        }
    }
}