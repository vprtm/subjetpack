package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.data.source.model.TvShow
import android.app.submission1jetpack.utils.DataDummy
import android.app.submission1jetpack.utils.Resource
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class TvShowDetailViewModelTest : KoinTest {

    private val viewModel by inject<TvShowDetailViewModel>()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val dummyTvShow = DataDummy.generateTvShow()

    @Mock
    private lateinit var catalogueRepository: CatalogueRepository

    @Before
    fun setUp() {
        Dispatchers.setMain(TestCoroutineDispatcher())

        startKoin {
            modules(
                module {
                    single {
                        TvShowDetailViewModel(catalogueRepository)
                    }
                }
            )
        }
    }

    @After
    fun tearDown() {
        stopKoin()
        Dispatchers.resetMain()
    }

    @Test
    fun `test get tv show detail`() {
        runBlockingTest {
            val tvShow = Resource.Success(dummyTvShow)
            whenever(catalogueRepository.getRemoteDetailTvShow(id = dummyTvShow.id)).thenReturn(tvShow)

            val actual = mutableListOf<Resource<TvShow>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                tvShow
            )

            viewModel.tvShowDetail.observeForever {
                actual.add(it)
            }

            viewModel.setTvShowId(dummyTvShow.id)

            viewModel.getTvShowDetail()

            Mockito.verify(catalogueRepository).getRemoteDetailTvShow(id = dummyTvShow.id)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test empty`() {
        runBlockingTest {
            val empty = Resource.empty(TvShow())
            whenever(catalogueRepository.getRemoteDetailTvShow(id = 0)).thenReturn(empty)

            val actual = mutableListOf<Resource<TvShow>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                empty
            )

            viewModel.tvShowDetail.observeForever {
                actual.add(it)
            }

            viewModel.setTvShowId(0)

            viewModel.getTvShowDetail()

            Mockito.verify(catalogueRepository).getRemoteDetailTvShow(id = 0)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test error`() {
        runBlockingTest {
            val error = Resource.Error("error")
            whenever(catalogueRepository.getRemoteDetailTvShow(id = 0)).thenReturn(error)

            val actual = mutableListOf<Resource<TvShow>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                error
            )

            viewModel.tvShowDetail.observeForever {
                actual.add(it)
            }

            viewModel.setTvShowId(0)

            viewModel.getTvShowDetail()

            Mockito.verify(catalogueRepository).getRemoteDetailTvShow(id = 0)

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test add favorite tv show`() {
        runBlockingTest {
            val tvShow = Resource.Success(dummyTvShow)
            whenever(catalogueRepository.insertFavoriteTvShow(tvShow.data)).thenReturn(tvShow)

            val actual = mutableListOf<Resource<TvShow>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                tvShow
            )

            viewModel.addFavorite.observeForever {
                actual.add(it)
            }

            viewModel.addToFavorite(tvShow.data)

            Mockito.verify(catalogueRepository).insertFavoriteTvShow(tvShow.data)

            Assert.assertEquals(expected, actual)
        }
    }

    @Test
    fun `test delete favorite tv show`() {
        runBlockingTest {
            val tvShow = Resource.Success(dummyTvShow)
            whenever(catalogueRepository.deleteFavoriteTvShow(tvShow.data)).thenReturn(tvShow)

            val actual = mutableListOf<Resource<TvShow>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                tvShow
            )

            viewModel.deleteFavorite.observeForever {
                actual.add(it)
            }

            viewModel.deleteFromFavorite(tvShow.data)

            Mockito.verify(catalogueRepository).deleteFavoriteTvShow(tvShow.data)

            Assert.assertEquals(expected, actual)
        }
    }

    @Test
    fun `test get favorite tv show`() {
        runBlockingTest {
            val success = Resource.success(true)
            whenever(catalogueRepository.getFavoriteTvShow(dummyTvShow.id)).thenReturn(success)

            val actual = mutableListOf<Resource<Boolean>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                success
            )

            viewModel.isFavorite.observeForever {
                actual.add(it)
            }

            viewModel.getFavorite(dummyTvShow.id)

            Mockito.verify(catalogueRepository).getFavoriteTvShow(dummyTvShow.id)

            Assert.assertEquals(expected, actual)
        }
    }
}