package android.app.submission1jetpack.viewmodel

import android.app.submission1jetpack.data.repo.helper.CatalogueRepository
import android.app.submission1jetpack.data.source.model.Movie
import android.app.submission1jetpack.utils.DataDummy
import android.app.submission1jetpack.utils.Resource
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.nhaarman.mockitokotlin2.whenever
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotNull
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runBlockingTest
import kotlinx.coroutines.test.setMain
import org.junit.*
import org.junit.runner.RunWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import org.koin.dsl.module
import org.koin.test.KoinTest
import org.koin.test.inject
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner

@ExperimentalCoroutinesApi
@RunWith(MockitoJUnitRunner::class)
class MovieViewModelTest : KoinTest {

    private val viewModel by inject<MovieViewModel>()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var catalogueRepository: CatalogueRepository

    @Mock
    private lateinit var observer: Observer<PagedList<Movie>>

    @Mock
    private lateinit var pagedList: PagedList<Movie>

    @Before
    fun setUp() {
        Dispatchers.setMain(TestCoroutineDispatcher())

        startKoin {
            modules(
                module {
                    single {
                        MovieViewModel(catalogueRepository)
                    }
                }
            )
        }
    }

    @After
    fun tearDown() {
        stopKoin()
        Dispatchers.resetMain()
    }

    @Test
    fun `test get movies`() {
        runBlockingTest {
            val movies = Resource.Success(DataDummy.generateMovies())
            whenever(catalogueRepository.getRemoteListMovie()).thenReturn(movies)

            val actual = mutableListOf<Resource<List<Movie>>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                movies
            )

            viewModel.movies.observeForever {
                actual.add(it)
            }

            viewModel.getMovies()

            verify(catalogueRepository).getRemoteListMovie()

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test empty`() {
        runBlockingTest {
            val empty = Resource.empty(listOf<Movie>())
            whenever(catalogueRepository.getRemoteListMovie()).thenReturn(empty)

            val actual = mutableListOf<Resource<List<Movie>>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                empty
            )

            viewModel.movies.observeForever {
                actual.add(it)
            }

            viewModel.getMovies()

            verify(catalogueRepository).getRemoteListMovie()

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `test error`() {
        runBlockingTest {
            val error = Resource.Error("error")
            whenever(catalogueRepository.getRemoteListMovie()).thenReturn(error)

            val actual = mutableListOf<Resource<List<Movie>>>()

            val expected = listOf(
                Resource.init(),
                Resource.loading(),
                error
            )

            viewModel.movies.observeForever {
                actual.add(it)
            }

            viewModel.getMovies()

            verify(catalogueRepository).getRemoteListMovie()

            assertEquals(expected, actual)
        }
    }

    @Test
    fun `get favorite movies`() {
        val dummyCourses = pagedList
        whenever(dummyCourses.size).thenReturn(5)
        val movies = MutableLiveData<PagedList<Movie>>()
        movies.value = dummyCourses

        whenever(catalogueRepository.getAllFavoriteMovies()).thenReturn(movies)
        val courseEntities = viewModel.getAllFavoriteMovies().value
        verify(catalogueRepository).getAllFavoriteMovies()
        assertNotNull(courseEntities)
        assertEquals(5, courseEntities?.size)

        viewModel.getAllFavoriteMovies().observeForever(observer)
        verify(observer).onChanged(dummyCourses)
    }
}