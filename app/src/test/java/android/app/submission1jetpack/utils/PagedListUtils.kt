package android.app.submission1jetpack.utils

import androidx.paging.PagedList
import com.nhaarman.mockitokotlin2.whenever
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mockito.mock

object PagedListUtils{
    fun <T> mockPagedList(list: List<T>): PagedList<T> {
        val pagedList = mock(PagedList::class.java) as PagedList<T>
        whenever(pagedList[anyInt()]).then { invocation ->
            val index = invocation.arguments.first() as Int
            list[index]
        }
        whenever(pagedList.size).thenReturn(list.size)

        return pagedList
    }
}
